# Trekksoft SDK

## Overview

This is PHP SDK for [Trekksoft API](http://developer.trekksoft.com/).

## Installation

To install SDK into your project you need to use composer (see https://getcomposer.org/). And then run:

`composer require trekksoft/sdk`


## Usage


### Basic Setup

```
$credentials = [
    'clientId'      => '<your-client-id>',
    'clientSecret'  => '<your-client-secret>',
];

$provider = new \TrekkSoft\SDK\OAuth2\Provider\TrekkSoftProvider($credentials);


//Get activities list
$activities = $provider->getActivities();

//Get availabilities list
$availabilities = $provider->getAvailabilities();

//Get merchants a list of merchants which you can access 
$merchants  = $provider->getMerchants();
```

### Demo

Check `demo` folder for example of usage of this SDK. To make it running you will 
need to copy `config.inc.php.dist` to `config.inc.php` and configure your client id and client secret in the new file.

Then you can put these files under your webroot folder and run it though web server. E.g. `http://localhost/trekksoft/activities.php`

### Models

All the data received from API is automatically wrapped into objects to be able to work with data easier.
You can check these models under `src/Model` folder. High level models are `Activity` and `Availability`. 
Others are nested under these models.
 
So for example to get occupancy of the availability item you need to do the following: `$availability->getCapacityInfo()->getOccupancy()`.

### Filter criteria

When making requests you can can use criteria objects for setting filter options:

- `TrekkSoft\SDK\Criteria\ActivityCriteria` for activities
- `TrekkSoft\SDK\Criteria\AvailabilityCriteria` for availabilities
- `TrekkSoft\SDK\Criteria\MerchantCriteria` for merchants

E.g. the following will only return all activities of bus2alps merchant which contain "rome":

```
$criteria = new \TrekkSoft\SDK\Criteria\ActivityCriteria();
$criteria->setMerchant('bus2alps');
$criteria->setTitleLike('rome');
```

Availability Criteria example:
```
$criteria = new \TrekkSoft\SDK\Criteria\AvailabilityCriteria();
$criteria->setMerchant('demo');
$criteria->resetSeats()->resetBookingTime()->setStartsFrom(null)->setWithGuests(true);
```

### Iterators

Each of the requests to provider returns [iterator](http://php.net/manual/en/class.iterator.php) as a result rather than direct array of the requested objects.
This is usable to automatically handle pagination and avoid sending large chunks of data. As you loop through iterator additional requests might be sent to the server as needed.
If you want to get a flat array still, use [`iterator_to_array()`](http://php.net/manual/en/function.iterator-to-array.php) method.

## Maintainers

This project is maintained by TrekkSoft Team.