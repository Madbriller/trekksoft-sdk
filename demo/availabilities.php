<?php
/**
 * @var TrekkSoft\SDK\OAuth2\Provider\TrekkSoftProvider $provider
 */
$provider = require 'bootstrap.inc.php';

$criteria = new \TrekkSoft\SDK\Criteria\AvailabilityCriteria();
$criteria->setMerchant('demo');
//$criteria->setOffset(5);
//$criteria->setLimit(17);
//$criteria->setWithActivities(true);
$criteria->resetSeats()->resetBookingTime()->setStartsFrom(null)->setWithGuests(true);

$availabilities = $provider->getAvailabilities($criteria);

printf("<h2>Found Availabilities:</h2>");

$total = 0;
foreach ($availabilities as $key => $availability) {
    ++$total;
    printf(
        "*%s*: <b>%s %s</b> <small>%s</small>: %s",
        $key,
        $availability->getStartDate(),
        $availability->getStartTime(),
        $availability->getId(),
        $availability->getActivityId() . ': ' . $availability->getActivityTitle()
    );

    if ($criteria->isWithGuests()) {
        $guests = $availability->getGuests();
        echo '<h3>Guests</h3>';
        foreach ($guests as $guest) {
            printf(
                "<p>%s - %s (%s) - %s.  #### %s</p>",
                $guest->getOccupancy() . ' x ' . $guest->getId(),
                $guest->getPrice()->getAmount() . ' ' . $guest->getPrice()->getCurrency()->getName(),
                $guest->getPricePerSeat()->getAmount() . ' ' . $guest->getPricePerSeat()->getCurrency()->getName(),
                $guest->getMultiDiscount()->getAmount() . ' ' . $guest->getMultiDiscount()->getCurrency()->getName(),
                $guest->getPriceCategory()->getId() . ' ' . $guest->getPriceCategory()->getName()
            );
        }
    }
    echo '<hr/><br/>';
}

echo "<h3>Total: $total</h3>";
