<?php
/**
 * @var TrekkSoft\SDK\OAuth2\Provider\TrekkSoftProvider $provider
 */
$provider = require 'bootstrap.inc.php';

$criteria = new \TrekkSoft\SDK\Criteria\BookingCriteria();
$criteria->setMerchant('bus2alps');
$criteria->setLimit(5);

/**
 * @var \TrekkSoft\SDK\Model\Booking[] $bookings
 */
$bookings = $provider->getBookings($criteria);

foreach ($bookings as $booking) {
    printf("<h2>Booking #%s at %s</h2>", $booking->getId(), $booking->getCreatedAt()->format(DateTime::ISO8601));

    $items = $booking->getItems();
    foreach ($items as $item) {
        printf("<h3>%s</h3>", $item->getCaption());

        echo '<ul>';
        $guests = $item->getGuests();
        foreach ($guests as $guest) {
            printf("<li>%s</li>", $guest->getName());
        }
        echo '</ul>';
    }
}