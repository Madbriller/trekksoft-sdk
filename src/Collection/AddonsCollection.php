<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\Addon;

/**
 * Class AddonsCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class AddonsCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return Addon::class;
    }
}
