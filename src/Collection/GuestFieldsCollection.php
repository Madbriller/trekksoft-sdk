<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\GuestField;

/**
 * Class GuestFieldsCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class GuestFieldsCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return GuestField::class;
    }
}
