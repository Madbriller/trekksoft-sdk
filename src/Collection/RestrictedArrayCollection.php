<?php
namespace TrekkSoft\SDK\Collection;

use Doctrine\Common\Collections\ArrayCollection;

abstract class RestrictedArrayCollection extends ArrayCollection
{
    /**
     * {@inheritDoc}
     */
    public function __construct(array $elements)
    {
        foreach ($elements as $element) {
            $this->ensureSupportedElement($element);
        }

        parent::__construct($elements);
    }

    /**
     * {@inheritDoc}
     */
    public function set($key, $value)
    {
        $this->ensureSupportedElement($value);

        parent::set($key, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function add($value)
    {
        $this->ensureSupportedElement($value);

        return parent::add($value);
    }

    /**
     * @param mixed $element
     * @return bool
     */
    abstract protected function ensureSupportedElement($element);
}
