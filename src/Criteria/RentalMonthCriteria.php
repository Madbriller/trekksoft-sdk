<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class ActivityCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class RentalMonthCriteria implements MerchantAwareCriteria
{
    use MerchantTrait;

    /**
     * @var int
     */
    private $activityId;

    /**
     * @var string
     */
    private $date;

    public function __construct($activityId = null, \DateTime $date = null)
    {
        $this->date = ($date ?: new \DateTime())->format('Y-m-d');
        $this->activityId = $activityId;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];
        $params += $this->getMerchantParams();

        if ($this->activityId) {
            $params['activityId'] = [$this->activityId];
        }

        if ($this->date) {
            $params['date'] = $this->date;
        }

        return $params;
    }
}
