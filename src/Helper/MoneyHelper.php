<?php
namespace TrekkSoft\SDK\Helper;

use Money\Money;
use Money\Currency;

/**
 * Class MoneyHelper
 * @package TrekkSoft\SDK\Helper
 */
class MoneyHelper
{
    const PRECISION = 100; //scale=2

    /**
     * @param array $data
     * @return Money
     */
    public static function createFromArray(array $data)
    {
        return self::createFromDecimal($data['amount'],  new Currency($data['currency']));
    }

    /**
     * @param string|float $amount
     * @param Currency $currency
     * @param integer $precision
     *
     * @return Money
     */
    public static function createFromDecimal($amount, Currency $currency, $precision = null)
    {
        $precision = $precision ?: self::PRECISION;

        return new Money((int)bcmul($amount, $precision), $currency);
    }
}
