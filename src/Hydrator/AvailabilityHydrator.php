<?php
namespace TrekkSoft\SDK\Hydrator;

use TrekkSoft\SDK\Model\Availability;

/**
 * Class AvailabilityHydrator
 * @package TrekkSoft\SDK\Hydrator
 */
class AvailabilityHydrator implements HydratorInterface
{
    /**
     * @param array $item
     * @return Availability
     */
    public function hydrate(array $item)
    {
        return new Availability($item);
    }
}
