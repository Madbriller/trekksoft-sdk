<?php
namespace TrekkSoft\SDK\Hydrator;
use TrekkSoft\SDK\Model\RentalAvailability;

/**
 * Class RentalAvailableHydrator
 * @package TrekkSoft\SDK\Hydrator
 */
class RentalAvailableHydrator implements HydratorInterface
{
    /**
     * @param array $item
     * @return RentalAvailability
     */
    public function hydrate(array $item)
    {
        return new RentalAvailability($item);
    }
}
