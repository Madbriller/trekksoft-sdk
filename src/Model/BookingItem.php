<?php
namespace TrekkSoft\SDK\Model;

use TrekkSoft\SDK\Collection\GuestsCollection;
use TrekkSoft\SDK\Helper\MoneyHelper;
use Money\Money;
use Money\Currency;

/**
 * Class BookingItem
 * @package TrekkSoft\SDK\Model
 */
class BookingItem
{
    /**
     * @var array
     */
    protected $options;

    /**
     * BookingItem constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'               => null,
            'caption'          => null,
            'fullCaption'      => null,
            'currency'         => null,
            'price'            => null,
            'multidiscount'    => null,
            'activity'         => null,
            'availabilityItem' => null,
            'isCancelled'      => false,
            'guests'           => [],
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getCaption()
    {
        return $this->options['caption'];
    }

    /**
     * @return string
     */
    public function getFullCaption()
    {
        return $this->options['fullCaption'];
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return (bool)$this->options['isCancelled'];
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return MoneyHelper::createFromDecimal($this->options['price'],  new Currency($this->options['currency']));
    }

    /**
     * @return Money
     */
    public function getPackageDiscount()
    {
        return MoneyHelper::createFromDecimal($this->options['multidiscount'],  new Currency($this->options['currency']));
    }

    /**
     * @return Activity
     */
    public function getActivity()
    {
        return new Activity($this->options['activity']);
    }

    /**
     * @return Availability
     */
    public function getAvailabilityItem()
    {
        return new Availability($this->options['availabilityItem']);
    }

    /**
     * @return GuestsCollection | Guest[]
     */
    public function getGuests()
    {
        $guests = new GuestsCollection([]);

        foreach ($this->options['guests'] as $guestData) {
            $guest = new Guest($guestData);
            $guests->add($guest);
        }

        return $guests;
    }
}
