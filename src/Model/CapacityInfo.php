<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class CapacityInfo
 * @package TrekkSoft\SDK\Model
 */
class CapacityInfo
{
    /**
     * @var int | null
     */
    private $availableSeats;

    /**
     * @var int | null
     */
    private $capacity;

    /**
     * @var int
     */
    private $occupancy;

    /**
     * @var int
     */
    private $occupancyPaid;

    /**
     * CapacityInfo constructor.
     * @param int $availableSeats
     * @param int $capacity
     * @param int $occupancy
     * @param int $occupancyPaid
     */
    public function __construct($availableSeats, $capacity, $occupancy = 0, $occupancyPaid = 0)
    {
        $this->availableSeats = ($availableSeats === null) ?: (int)$availableSeats;
        $this->capacity       = ($capacity === null) ?: (int)$capacity;
        $this->occupancy      = (int)$occupancy;
        $this->occupancyPaid  = (int)$occupancyPaid;
    }

    /**
     * @return int|null
     */
    public function getAvailableSeats()
    {
        return $this->availableSeats;
    }

    /**
     * @param int|null $availableSeats
     */
    public function setAvailableSeats($availableSeats)
    {
        $this->availableSeats = $availableSeats;
    }

    /**
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * @return int
     */
    public function getOccupancy()
    {
        return $this->occupancy;
    }

    /**
     * @return int
     */
    public function getOccupancyPaid()
    {
        return $this->occupancyPaid;
    }
}
