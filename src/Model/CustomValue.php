<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class CustomValue
 * @package TrekkSoft\SDK\Model
 */
class CustomValue
{
    /**
     * @var array
     */
    protected $options;

    /**
     * CustomValue constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'key'        => null,
            'value'      => null,
            'type'       => null,
            'label'      => null,
        ];
        
        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->options['key'];
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->options['value'];
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->options['type'];
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->options['label'];
    }

}
