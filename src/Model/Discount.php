<?php
namespace TrekkSoft\SDK\Model;

use TrekkSoft\SDK\Helper\MoneyHelper;
use Money\Money;

class Discount
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Discount constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'code'       => null,
            'percentage' => null,
            'amount'     => null,
            'currency'   => null,
        ];

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->options['code'];
    }

    /**
     * @return float
     */
    public function getPercentage()
    {
        return (float)$this->options['percentage'];
    }

    /**
     * @return Money
     */
    public function getValue()
    {
        return MoneyHelper::createFromArray($this->options);
    }
}
