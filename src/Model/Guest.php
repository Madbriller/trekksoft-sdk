<?php
namespace TrekkSoft\SDK\Model;

use TrekkSoft\SDK\Collection\AddonsCollection;
use TrekkSoft\SDK\Collection\CustomValuesCollection;
use TrekkSoft\SDK\Collection\GuestResourcesCollection;
use TrekkSoft\SDK\Helper\MoneyHelper;
use Money\Money;

/**
 * Class Guest
 * @package TrekkSoft\SDK\Model
 */
class Guest
{
    /**
     * @var array
     */
    protected $options;

    /**
     * PriceCategory constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'id' => null,
            'name' => null,
            'email' => null,
            'remarks' => null,
            'currency' => null,
            'price' => null,
            'pricePerSeat' => null,
            'multiDiscount' => 0,
            'barCode' => null,
            'occupancy' => null,
            'cancelation' => null,
            'priceCategory' => [
                'id'    => null,
                'name'  => null,
            ],
            'discount' => null,
            'customValues' => [],
            'basketPaymentStatus' => null,
            'addons' => [],
            'resources' => [],
            'isReservation' => false,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return bool
     */
    public function isReservation()
    {
        return (bool)$this->options['isReservation'];
    }

    /**
     * @return string
     */
    public function getBasketPaymentStatus()
    {
        return ($this->options['basketPaymentStatus'] !== null) ? (string)$this->options['basketPaymentStatus'] : null;
    }

    /**
     * @return PriceCategorySimple
     */
    public function getPriceCategory()
    {
        return new PriceCategorySimple($this->options['priceCategory']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return (string)$this->options['name'];
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return (string)$this->options['email'];
    }

    /**
     * @return string
     */
    public function getRemarks()
    {
        return (string)$this->options['remarks'];
    }

    /**
     * @return int
     */
    public function getOccupancy()
    {
        return (int)$this->options['occupancy'];
    }

    /**
     * @return string
     */
    public function getBarCode()
    {
        return (string)$this->options['barCode'];
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['price'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getPricePerSeat()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['pricePerSeat'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return Money
     */
    public function getMultiDiscount()
    {
        return MoneyHelper::createFromArray([
            'amount' => $this->options['multiDiscount'],
            'currency' => $this->options['currency'],
        ]);
    }

    /**
     * @return bool
     */
    public function hasDiscount()
    {
        return $this->options['discount'] ? true : false;
    }

    /**
     * @return Discount | null
     */
    public function getDiscount()
    {
        if ($discountData = $this->options['discount']) {
            $discountData['currency'] = $this->options['currency'];

            return new Discount($discountData);
        } else {
            return null;
        }
    }

    /**
     * @return bool
     */
    public function isCancelled()
    {
        return ($this->options['cancelation'] !== null);
    }

    /**
     * @return CustomValuesCollection | CustomValue[]
     */
    public function getCustomValues()
    {
        $customValues = [];

        foreach ($this->options['customValues'] as $customValue) {
            $customValues[] = new CustomValue($customValue);
        }

        return new CustomValuesCollection($customValues);
    }

    /**
     * @return AddonsCollection
     */
    public function getAddons()
    {
        $addons = [];

        foreach ($this->options['addons'] as $addonData) {
            $addons[] = new Addon($addonData);
        }

        return new AddonsCollection($addons);
    }

    /**
     * @return GuestResourcesCollection | GuestResource[]
     */
    public function getResources()
    {
        $resources = [];

        foreach ($this->options['resources'] as $resourceData) {
            $resources[] = new GuestResource($resourceData);
        }

        return new GuestResourcesCollection($resources);
    }
}
