<?php
declare(strict_types=1);

namespace TrekkSoft\SDK\Model;

class GuestResource
{
    protected $options;

    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    protected function setOptions(array $options)
    {
        $options += [
            'id' => null,
            'occupancy' => null,
        ];

        $this->options = $options;
    }

    public function getId()
    {
        return (int) $this->options['id'];
    }

    public function getOccupancy()
    {
        return (int) $this->options['occupancy'];
    }
}
