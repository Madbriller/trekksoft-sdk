<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class Location
 * @package TrekkSoft\SDK\Model
 */
class Location
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Country constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'country' => [
                'code' => null,
                'name' => null,
            ],
            'city' => [
                'id'     => null,
                'locode' => null,
                'name'   => null,
            ],
        ];

        $this->options = $options;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return new Country($this->options['country']);
    }

    /**
     * @return City
     */
    public function getCity()
    {
        return new City($this->options['city']);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf("%s, %s", $this->getCity()->getId().': '.$this->getCity()->getName(), $this->getCountry()->getName());
    }
}
