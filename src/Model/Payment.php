<?php
namespace TrekkSoft\SDK\Model;

use TrekkSoft\SDK\Helper\MoneyHelper;
use Money\Money;
use Money\Currency;

/**
 * Class Payment
 * @package TrekkSoft\SDK\Model
 */
class Payment
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Discount constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'id'                    => null,
            'treasurerType'         => null,
            'treasurerReference'    => null,
            'amount'                => null,
            'currency'              => null,
            'comment'               => null,
            'channel'               => null,
            'createdAt'             => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getTreasurerType()
    {
        return $this->options['treasurerType'];
    }

    /**
     * @return string
     */
    public function getTreasurerReference()
    {
        return $this->options['treasurerReference'];
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->options['comment'];
    }

    /**
     * @return string
     */
    public function getChannel()
    {
        return $this->options['channel'];
    }

    /**
     * @return Money
     */
    public function getValue()
    {
        return MoneyHelper::createFromDecimal($this->options['amount'],  new Currency($this->options['currency']));
    }

    /**
     * @return \DateTimeImmutable
     */
    public function getCreatedAt()
    {
        return new \DateTimeImmutable($this->options['createdAt']);
    }
}
