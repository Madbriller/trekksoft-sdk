<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class PriceCategorySimple
 * @package TrekkSoft\SDK\Model
 */
class PriceCategorySimple
{
    /**
     * @var array
     */
    protected $options;

    /**
     * PriceCategorySimple constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'id'             => null,
            'name'           => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->options['id'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->options['name'];
    }
}
