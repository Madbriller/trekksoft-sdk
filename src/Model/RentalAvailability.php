<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class Activity
 * @package TrekkSoft\SDK\Model
 */
class RentalAvailability
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Activity constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'           => null,
            'bookingNote'  => null,
            'startTimes'   => [],
            'startDate'    => null,
            'actualUntil'  => null,
            'duration'     => null,
            'itemDuration' => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->options['bookingNote'];
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->options['startDate'];
    }

    /**
     * @return string|null
     */
    public function getEndDate()
    {
        if ($this->options['actualUntil'] === null) {
            return null;
        }
        $endDate = \DateTime::createFromFormat('Y-m-d', $this->options['startDate'])
            ->add(new \DateInterval('P1M'));
        $actualUntil = \DateTime::createFromFormat(\DateTime::ISO8601, $this->options['actualUntil']);
        return ($actualUntil < $endDate ? $actualUntil : $endDate)->format('Y-m-d');
    }

    /**
     * @return array
     */
    public function getStartTimes()
    {
        return $this->options['startTimes'];
    }

    /**
     * @return string
     */
    public function getDuration()
    {
        return $this->options['duration'];
    }

    /**
     * @return string
     */
    public function getItemDuration()
    {
        return $this->options['itemDuration'];
    }
}
