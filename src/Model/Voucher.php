<?php
namespace TrekkSoft\SDK\Model;

use TrekkSoft\SDK\Helper\MoneyHelper;
use Money\Money;
use Money\Currency;

/**
 * Class Voucher
 * @package TrekkSoft\SDK\Model
 */
class Voucher
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Discount constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'code'       => null,
            'discount'   => null,
            'currency'   => null,
        ];

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->options['code'];
    }

    /**
     * @return Money
     */
    public function getValue()
    {
        return MoneyHelper::createFromDecimal($this->options['amount'],  new Currency($this->options['currency']));
    }
}
