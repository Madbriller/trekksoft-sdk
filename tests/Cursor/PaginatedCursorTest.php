<?php

namespace TrekkSoft\SDK\Cursor;

use League\OAuth2\Client\Provider\AbstractProvider;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;
use Iterator;
use TrekkSoft\SDK\Hydrator\ArrayHydrator;
use TrekkSoft\SDK\Hydrator\HydratorInterface;
use PHPUnit\Framework\TestCase;

class PaginatedCursorTest extends TestCase
{
    public function testBugWithInfiniteLoop()
    {
        $cursor = new PaginatedCursor(
            $this->getRequestStub(),
            $this->getProviderStub([['id' => 1]])
        );

        $this->assertCount(1, iterator_to_array($cursor));
    }

    private function getRequestStub()
    {
        $uri = $this->createMock(UriInterface::class);
        $uri->method('getQuery')
            ->willReturn('');

        $uri->method('withQuery')
            ->willReturn($uri);


        $request = $this->createMock(RequestInterface::class);
        $request->method('getUri')
            ->willReturn($uri);

        $request->method('withUri')
            ->willReturn($request);

        return $request;
    }

    private function getProviderStub(array $data)
    {
        $provider = $this->createMock(AbstractProvider::class);
        $provider->method('getResponse')
            ->willReturn($data);

        return $provider;
    }
}
